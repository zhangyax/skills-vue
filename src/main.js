import Vue from 'vue'
import App from './App.vue'
import VeeValidate from 'vee-validate';  

Vue.use(VeeValidate);  // Add this
Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
